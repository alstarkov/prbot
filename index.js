const octokit = require('@octokit/rest')()
const jsonwebtoken = require('jsonwebtoken');
const Observable = require('rxjs/Observable').Observable;
//const map = require('rxjs/operator/map').map;

const WORK_BRANCH = 'wireapp:fix/ZIOS-9762-List-UI-turns-black-during-transition-to-conversation-view';
                  // feature/2elevee-update

module.exports.getRootPodfiles = getRootPodfiles;
module.exports.handle = handle;

async function handle(repository, update) {
//  const pr = getPR('wireapp', 'wire-ios');

//  const branch = getBranch(pr);  // Is there a valid PR? => Work from branch

  const updatedPodfileLock = update(podfile, podfileLock);
  console.log('updatedPodfileLock:', updatedPodfileLock);
//  if(updates) {
    // create or update branch, create or update PR
    // TODO: add diff comments
//  }
}

function refreshToken() {
  const now = Math.floor(new Date() / 1000); // min
  const jwt = jsonwebtoken.sign({
      iat: now,
      exp: now + 60,
      iss: 'MY-----ISSUER_ID',
    },
    'MY---PEM',
    { algorithm: 'RS256' },
  );
  octokit.authenticate({
    type: 'integration',
    token: jwt
  });
  const token = await
  Rx.Observable.fromPromise(
    octokit.apps.createInstallationToken({
                    installation_id: installationId,
                  })
  ).subscribe(token =>
    octokit.authenticate({
                    type: 'token',
                    token: token
    })
  );
}

const fetchRootPodfiles = flatMap(repository =>
  Rx.Observable.fromPromise(
    octokit.repos.getContent({
      owner: repository.owner.login,
      repo: repository.name,
      path: 'Podfile'
  }))
);

const withAuth = retryWhen(error =>
    errors.scan(function (errorCount, err) {
      console.log('\n');
      console.log('scan inside of retryWhen. Error counter: ', errorCount);
      if (errorCount >= 1) { // TODO: || !401
          throw err;
      }
      refreshToken(); // TODO: ones per hour only
      return errorCount + 1;
    }, 1)
);

const fromBase64 = map(podfile => Buffer.from(podfile.data.content, 'base64').toString('utf8'))

const getRootPodfiles = pipe(
  fetchRootPodfiles,
  withAuth,
  fromBase64
);





async function getPR(owner, repo) {  // get active PR
 //  octokit.authenticate({
 //    type: 'integration',
 //    token: 'secrettoken123'
 //  })
  const prs = await octokit.pullRequests.getAll({
    owner: owner,
    repo: repo,
    head: WORK_BRANCH
  })
  // TODO: filter by default_branch
  // TODO: close if conflicts with default_branch (and default is still = base)
    // Or close PR, rm branch
  console.log('prbot PRs:', prs);
  return prs;
}

async function getBranch(pr) {  // 1. Which branch - master or existent feature?
  return '';
}

async function podsIterator() { // return pods one by one
  // TODO: find .2elevee and Podfiles, update received pods
  return '';
}
